import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  public name="Saurabh Mamidwar"
  getHello(): string {
    return `Your name is ${this.name}`;
  }
}
