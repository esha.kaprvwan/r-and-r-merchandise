export class MerchandiseDTO{
    id?: string;
    title:string;
    count:number;
    points:number;
    size: [string];
    description:string;
    category:string;
    createdAt:string;
    updatedAt:string
};