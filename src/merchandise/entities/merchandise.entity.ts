import { BaseEntity } from "src/base-entity"
import { Column, Entity, JoinColumn, OneToMany, OneToOne } from "typeorm";
import { Category } from "src/category/entities/category.entity";
import { Size } from "src/size/entities/size.entity";
@Entity('merchandise')
export class Merchandise extends BaseEntity {

        @Column({ type: 'varchar', length: 100, nullable: false })
        title: string

        @Column({ type : 'int' , nullable:false})
        count: number

        @Column({ type :'int' , nullable:false})
        points: number

        @OneToMany(() => Size, (size: Size) => size.size)
        @JoinColumn()          //foreign key
        sizes: Size[]

        @Column({ type: 'varchar' , nullable: true})
        description: string

        @OneToOne(() => Category, (category: Category) => category.category)
        @JoinColumn()        //foreign key
        category: Category

}

