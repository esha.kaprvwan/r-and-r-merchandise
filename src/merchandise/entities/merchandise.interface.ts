export interface Merchandise{
    id?: string;
    title:string;
    count:number;
    points:number;
    size: [string];
    description:string;
    category:string;
};