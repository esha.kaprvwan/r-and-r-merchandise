import { Body, Controller, Get, Post } from "@nestjs/common";
import { MerchandiseDTO } from "../dto/merchandise.model";
import { MerchandiseService } from "../services/merchandise.service";

@Controller('merchandise')
export class MerchandiseController{
    constructor(private merchandiseService : MerchandiseService){}
    
    @Get()
    async getAllMerchandise(){
        const merchandise= await this.merchandiseService.findAll().then(result=>{
            return result;
        }).catch((err)=>{
            return ("Some error occured: " +err);
        })
        console.log(merchandise);
        return merchandise;
    }
    
    @Post()
    async addMerchandise(@Body() merch:MerchandiseDTO){
        console.log(merch);
        const savedMerch =await this.merchandiseService.createMerchandise(merch);
        return { data : savedMerch }
    }
}