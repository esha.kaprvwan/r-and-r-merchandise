export class SizeDTO{
    id?: string;
    size:string;
    createdAt:string;
    updatedAt:string;
}