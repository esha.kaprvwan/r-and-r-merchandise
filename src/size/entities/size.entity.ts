import { BaseEntity } from "src/base-entity";
import { Entity, ManyToOne } from "typeorm";
import { Merchandise } from "src/merchandise/entities/merchandise.entity";

@Entity('Size')
export class Size extends BaseEntity {

        @ManyToOne(() => Merchandise, (merch: Merchandise) => merch.sizes)
        size: Merchandise

}

