export class CategoryDTO{
    id?: string;
    category:string;
    createdAt:string;
    updatedAt:string;
}