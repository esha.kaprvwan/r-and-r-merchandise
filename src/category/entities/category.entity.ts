import { BaseEntity } from "src/base-entity";
import { Entity, OneToOne } from "typeorm";
import { Merchandise } from "src/merchandise/entities/merchandise.entity";


@Entity('category')
export class Category extends BaseEntity {

        @OneToOne(() => Merchandise, (merch: Merchandise) => merch.category)
        category: Merchandise


}

