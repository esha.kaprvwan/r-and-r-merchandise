import { TypeOrmModuleOptions } from "@nestjs/typeorm";

export const config:TypeOrmModuleOptions={
    type:'postgres',
    port:5432,
    host:'127.0.0.1',
    database:'merchandise',
    synchronize:true,
    username: "postgres",
    password: "Esha@2001",
    entities: ["dist/**/*.entity{.ts,.js}"]
}